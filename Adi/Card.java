package com.company;
// Card class represents a playing card.

enum Face {
   Ace, Deuce, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King
}

enum Suit{
   Hearts, Diamonds, Clubs, Spades
}

public class Card 
{
   private Face face; // face of card ("Ace", "Deuce", ...)
   private Suit suit; // suit of card ("Hearts", "Diamonds", ...)

   // two-argument constructor initializes card's face and suit
   public Card( String cardFace, String cardSuit )
   {
      face = Face.valueOf(cardFace);
      suit = Suit.valueOf(cardSuit);
   }

   boolean isGreaterThan(Card other) {
      return this.face.ordinal() > other.face.ordinal();
   }

   boolean isEqualsTo(Card that) {
      return this.face.equals(that.face) && this.suit.equals(that.suit);
   }

   // return String representation of Card
   public String toString() {
      return face + " of " + suit;
   }
}
