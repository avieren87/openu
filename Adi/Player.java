package com.company;

public class Player {

    private DeckOfCards deckOfCards;

    public Player(){
        deckOfCards = new DeckOfCards(0);
    }

    public void addCardToDeck(Card card){
        deckOfCards.addCard(card);
    }
    public Card dealCard(){
        return deckOfCards.dealCard();
    }
}
