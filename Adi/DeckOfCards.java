package com.company;
// DeckOfCards class represents a deck of playing cards.
import java.util.Random;
import java.util.ArrayList;


public class DeckOfCards
{
   private ArrayList<Card> deck = new ArrayList(); // array of Card objects
   private int currentCard = 0;
   private static final Random randomNumbers = new Random();  // random number generator


   public DeckOfCards(int number_of_cards)
   {
      String[] faces = { "Ace", "Deuce", "Three", "Four", "Five", "Six",
              "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King" };
      String[] suits = { "Hearts", "Diamonds", "Clubs", "Spades" };

      // populate deck with Card objects
      for ( int count = 0; count < number_of_cards; count++ )
         deck.add(new Card( faces[ count % 13 ], suits[ count / 13 ] ));
   }

   // shuffle deck of Cards with one-pass algorithm
   // after shuffling, dealing should start at deck[ 0 ] again
   public void shuffle()
   {
      // reinitialize currentCard
      currentCard = 0;

      // for each Card, pick another random Card (0-51) and swap them
      for ( int first = 0; first < deck.size(); first++ )
      {
         // select a random number
         int random =  randomNumbers.nextInt( deck.size()-1);
         
         // swap current Card with randomly selected Card
         Card temp = deck.get(first);
         deck.remove(first);
         deck.add(first,deck.get(random));
         deck.remove(random);
         deck.add(random,temp);
      }
   }

   // deal one Card
   public Card dealCard()
   {
      // determine whether Cards remain to be dealt
      if ( currentCard < deck.size() )
         // return current Card
         return deck.get(currentCard++);

      // all Cards were dealt
      return null;
   }

   public void addCard(Card card){
      deck.add(deck.size(),card);
   }

   public int size(){
      return deck.size();
   }

}

