package com.company;

public class CardsWarGame
{
   private DeckOfCards deckOfCards =  new DeckOfCards(52);
   private Player[] players;

   public CardsWarGame(int number_of_players){
      players = new Player[number_of_players]; // create static array of player objects

      for ( int i = 0; i < players.length; i++ )
         players[i] = new Player();

      deckOfCards.shuffle(); // place Cards in random order
      divideDeck();
   }

   public void turn() {

      Player currentWinner;
      Card [] turnCards = getPlayersCards();

      if (isWar(turnCards))
         this.war();
      else {
         for (int i = 0; i < turnCards.length - 1; i++)
            if (turnCards[i].isGreaterThan(turnCards[i++])) {
               currentWinner = players[i];
            }
      }
   }

   private boolean isWar(Card playersCards[]){
         for (int i = 0; i<playersCards.length-1; i++){
            if (!(playersCards[i].isEqualsTo(playersCards[i++])))
            return false;
      }
      return true;
   }

   private Card [] getPlayersCards(){

      Card [] turnCards = new Card[players.length];

      for (int i=0; i < players.length; i++)
         turnCards[i] = players[i].dealCard();

      return turnCards;
   }

   private void divideDeck(){
      for ( int i = 0; i < deckOfCards.size(); i++ )
         players[i%players.length].addCardToDeck(deckOfCards.dealCard());
   }

   private void war(){
   }

}
