import java.util.ArrayList;

class Player {
    private static final int HEAD = 0;
    private ArrayList<Card> playerDeck;

    Player(ArrayList<Card> cards) {
        this.playerDeck = cards;
    }

    //add card to player deck
    void addCard(Card card) {
        playerDeck.add(card);
    }

    //draw card from player deck
    Card drawCard() {
        Card tmpCard = playerDeck.get(HEAD);
        playerDeck.remove(HEAD);
        return tmpCard;
    }

    //check if player is out of cards
    Boolean isDeckEmpty() {
        return this.playerDeck.isEmpty();
    }

    //return the number of player's deck
    int playerState() {
        return playerDeck.size();
    }

}
