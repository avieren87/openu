public class Card {

    private final Integer face;
    private final String suit;

    Card(Integer cardFace, String cardSuit) {
        this.face = cardFace;
        this.suit = cardSuit;
    }

    public String toString() {
        return face + " of " + suit;
    }

    Integer getValue() {
        return face;
    }

    String getSuit() {
        return suit;
    }

}
