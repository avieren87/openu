import java.util.ArrayList;

class Game {

    private static Dialog dialog = new Dialog();
    private static DeckOfCards myDeckOfCards = new DeckOfCards();
    private static Player player1 = new Player(myDeckOfCards.dealHalfDeck());  //half shuffled deck to player1
    private static Player player2 = new Player(myDeckOfCards.dealHalfDeck());  //half shuffled deck to player2
    private static ArrayList<Card> roundPile = new ArrayList<>();              //contains all draw cards each round.

    //main function that start the game
    void game() {
        while (!isSomeoneEmpty()) {
            whoIsHigher();
        }
    }

    //true if one of the players have empty deck else return false.
    private static boolean isSomeoneEmpty() {
        boolean flag = false;
        if (player1.isDeckEmpty() && !player2.isDeckEmpty()) {
            dialog.winner(2); //announce player2 wins
            flag = true;
        } else if (!player1.isDeckEmpty() && player2.isDeckEmpty()) {
            dialog.winner(1); //announce player1 wins
            flag = true;
        } else if (player1.isDeckEmpty() && player2.isDeckEmpty()) {
            dialog.winner(0); //announce games ends with draw
            flag = true;
        }
        return flag;
    }

    //add cards to the winner deck each round.
    private static void updateDeck(boolean playerOneWin) {
        if (playerOneWin) {
            for (Card temp : roundPile) {
                player1.addCard(temp);
            }
        } else {
            for (Card temp : roundPile) {
                player2.addCard(temp);
            }
        }
    }

    //return 14 if card value is 1 (Ace)
    private static Integer isAce(int cardValue) {
        if (cardValue == 1) {
            return 14;
        } else {
            return cardValue;
        }
    }

    //makes the card comparison, in case of war issue war and updates the winners deck.
    private static void whoIsHigher() {

        boolean warIssued = false;
        roundPile.clear();                                   //clear pile each turn
        Card card1 = player1.drawCard();                     //card player1 initiate instance
        Card card2 = player2.drawCard();                     //card player2 initiate instance
        roundPile.add(card1);
        roundPile.add(card2);
        Integer c1 = isAce(card1.getValue());           //holds cards int value
        Integer c2 = isAce(card2.getValue());           //holds cards int value

        while (c1.equals(c2)) {
            warIssued = true;
            for (int i = 0; i < 3; i++) {

                //check if someone is out of cards
                if (isSomeoneEmpty()) {
                    return;
                }

                card1 = player1.drawCard();
                card2 = player2.drawCard();
                roundPile.add(card1);
                roundPile.add(card2);
            }
            c1 = isAce(card1.getValue());
            c2 = isAce(card2.getValue());
        }

        if (c1 > c2) {
            updateDeck(true);
            dialog.state(1, player1.playerState(), player2.playerState(), roundPile, warIssued);
        } else {
            updateDeck(false);
            dialog.state(2, player1.playerState(), player2.playerState(), roundPile, warIssued);
        }

    }

}
