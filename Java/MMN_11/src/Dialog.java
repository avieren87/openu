import javax.swing.JOptionPane;
import java.util.ArrayList;

class Dialog {
    private static final int HEAD = 0;

    //display state about each turn in the game.
    void state(int winner, int p1size, int p2size, ArrayList<Card> pile, boolean war) {
        StringBuilder msg = new StringBuilder("Round summery:\n\n");
        if (war) {
            msg.append("War issued!\n\n");
        }
        for (; !pile.isEmpty(); ) {
            msg.append(String.format("Player1 card number is : %d %s \nPlayer2 card number is : %d %s\n\n", pile.get(0).getValue(), pile.get(0).getSuit(), pile.get(1).getValue(), pile.get(1).getSuit()));
            pile.remove(HEAD);
            pile.remove(HEAD);
        }

        msg.append(String.format("the winner for this round is player: %d\n\n", winner));
        msg.append(String.format("Player1 deck contains: %d\nPlayer2 deck contains: %d", p1size, p2size));
        JOptionPane.showMessageDialog(null, msg);
    }

    //announce about the winner of the game.
    void winner(int winner) {
        String msg;
        if (winner == 0) {
            msg = "Game ends in draw!";
        } else {
            msg = "The winner for this game is:\nPlayer" + winner + "!\n";
        }
        JOptionPane.showMessageDialog(null, msg);
    }
}
